<?php declare(strict_types=1);


namespace DataAccess;


class ModelA
{
    private \Models\ModelA $modelA;
    
    /**
     * ModelA constructor.
     */
    public function __construct(\Models\ModelA $modelA)
    {
        $this->modelA = $modelA;
    }
}
