<?php declare(strict_types=1);


namespace Services;


use Models\ModelA;

class MockServiceA
{
    private ModelA $modelA;


    /**
     * MockServiceA constructor.
     * @Annotation(ModelA::class)
     */
    public function __construct(ModelA $modelA)
    {
        $this->modelA = $modelA;
    }
}
